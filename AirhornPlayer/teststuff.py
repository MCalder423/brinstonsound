__author__ = 'mcalder'
import mod_playwav
import FileUtils

# Test the TexToSpeech module
import TextToSpeech
TextToSpeech.SayThis('Piper down! We have a piper down over here!  No, it\'s okay, he\'s just pissed.')

# Here's a cheaty way to do multi-threading.  USe a seperate subprocess for each thread using this code
# Got it from here: http://www.raspberrypi.org/phpBB3/viewtopic.php?t=23044&p=216151
# import subprocess
#
#
# def say(something, language='en', voice='f2'):
#     subprocess.call(['espeak', '-v%s+%s' % (language, voice), something])

# import FileUtils
# airhornDrive = FileUtils.FindAirhornSoundDrive()
# print airhornDrive

# mod_playwav.play(1, '/home/pi/brinstonsound/AirhornPlayer/SystemSounds/Austin_Powers_groovy.wav', 100, 0)

