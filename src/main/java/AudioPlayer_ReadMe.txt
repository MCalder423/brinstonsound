Playing an audio file (advanced)

Purpose.  Plays a single audio file. Capable of playing some compressed audio formats (A-law, μ-law, maybe ogg vorbis, mp3, GSM06.10). Allows control over buffering and which mixer to use.

Usage.

java AudioPlayer -l

java AudioPlayer [ -M mixername ] [ -e buffersize ] [ -i buffersize ] audiofile

Parameters.

-h
print usage message

-l
lists the available mixers

-M mixername
selects a mixer to play on

-e buffersize
the buffer size to use in the application ("extern")

-i buffersize
the buffer size to use in Java Sound ("intern")

-E endianess
the endianess ("big" or "little") to use in conversions. The default is little. Specifying this option forces a conversion, even if the audio format is supported by SourceDataLines directly.

-S sample size
the sample size in bits to use in conversions. The default is 16. Specifying this option forces a conversion, even if the audio format is supported by SourceDataLines directly.

-D
enable debugging output

-f
interpret filename arguments as filenames. This is the default. This option is exclusive to -u.

-u
interpret filename arguments as URLs. The default is to interpret them as filenames. This option is exclusive to -f.

audiofile
the file name of the audio file to play

Bugs, limitations.  Compressed formats can be handled depending on the capabilities of the Java Sound implementation. A-law and μ-law can be handled in any known Java Sound implementation. Ogg vorbis, mp3 and GSM 06.10 can be handled by Tritonus. If you want to play these formats with the Sun jdk1.3/1.4, you have to install the respective plug-ins from Tritonus Plug-ins.

Source code.  AudioPlayer.java, AudioCommon.java, gnu.getopt.Getopt

