import javax.speech.*;
import javax.speech.synthesis.*;
import java.util.Locale;

/**
 * Created by pi on 24/09/16.
 */
public class TextSpeaker {
    public static void SayThis(String text) {

        try
        {
            // Create a synthesizer for English
            Synthesizer synth = Central.createSynthesizer(new SynthesizerModeDesc(Locale.ENGLISH));



            // Get it ready to speak
            synth.allocate();
            synth.resume();

            // Speak the supplied string
            synth.speakPlainText(text, null);

            // Wait till speaking is done
            synth.waitEngineState(Synthesizer.QUEUE_EMPTY);

            // Clean up
            synth.deallocate();
        } catch (
                Exception e)

        {
            e.printStackTrace();
        }

    }

    public static void speech(String text) {
        if (text == null || text.trim().isEmpty()) return;

        String voiceName = "kevin16";

        try {
            System.setProperty("FreeTTSSynthEngineCentral", "com.sun.speech.freetts.jsapi.FreeTTSEngineCentral");
            System.setProperty("freetts.voices", "com.sun.speech.freetts.en.us.cmu_us_kal.KevinVoiceDirectory");
            Central.registerEngineCentral("com.sun.speech.freetts.jsapi.FreeTTSEngineCentral");

            SynthesizerModeDesc desc = new SynthesizerModeDesc(null, "general", Locale.US, null, null);

            Synthesizer synth = Central.createSynthesizer(desc);
            synth.allocate();
            desc = (SynthesizerModeDesc) synth.getEngineModeDesc();
            Voice[] voices = desc.getVoices();
            Voice voice = null;
            for (Voice entry : voices) {
                if(entry.getName().equals(voiceName)) {
                    voice = entry;
                    break;
                }
            }
            synth.getSynthesizerProperties().setVoice(voice);
            synth.resume();
            synth.speakPlainText(text, null);
            synth.waitEngineState(Synthesizer.QUEUE_EMPTY);
            synth.deallocate();

        } catch(Exception ex) {
            String message = " missing speech.properties in " + System.getProperty("user.home") + "\n";
            System.out.println("" + ex);
            System.out.println(message);
            ex.printStackTrace();
        }
    }
}
